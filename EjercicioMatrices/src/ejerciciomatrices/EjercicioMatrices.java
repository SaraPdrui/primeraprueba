package ejerciciomatrices;

import java.util.Scanner;

public class EjercicioMatrices {    
  
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce el número de filas de la matriz:");
        int fila = sc.nextInt();
        System.out.println("Introduce el número de columnas de la matriz:");
        int col = sc.nextInt();
        System.out.println(""); 
        int [][] matriz = new int[fila][col];
        System.out.println("Primer array:");
        rellenar(matriz);
        System.out.println("----------------");
        System.out.println("Segundo array:");
        rellenarAlReves(matriz);
        System.out.println("----------------");
        System.out.println("Tercer array:");
        rellenarMult3(matriz);
    }
    
    public static void rellenar(int[][] matriz) {
        int i, j, cero = 0;
        for (i = 0; i < matriz.length; i++) {
            for (j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = cero;
                cero++;
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public static void rellenarMult3(int[][] matriz) {
        int mul = 3;
        for (int i = 0; i < matriz.length; i++){
            for (int j = 0; j < matriz[i].length; j++){
              matriz[i][j] = mul;
              mul += 3;
                System.out.print(matriz[i][j] + " ");
              }
            System.out.println("");            
            }
        }
    
    public static void rellenarAlReves(int[][] matriz) {
        int i, j;
        for (i = matriz.length - 1; i >= 0; i--) {
            for (j = matriz.length; j >= 0; j--) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
    }
}

