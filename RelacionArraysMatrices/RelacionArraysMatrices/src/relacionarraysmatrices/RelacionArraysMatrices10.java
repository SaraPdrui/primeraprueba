package relacionarraysmatrices;

import java.util.Scanner;

public class RelacionArraysMatrices10 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        int [] array = new int [n];
        System.out.println("--------------");
        System.out.println(imprimeArray(array));
    }
    
    public static String imprimeArray(int [] array) {
        String devolver = "";
        for (int i = 0; i < array.length; i++) {
        	devolver += array[i] + " ";
        }
        return devolver;
    }
    
}
