package relacionarraysmatrices;

import java.util.Scanner;

public class RelacionArraysMatrices4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        char [] array = new char [n];
        System.out.println("--------------");
        for (int i = 0; i < array.length; i++) {
            array[i] = 'a';
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }
    
}
