package relacionarraysmatrices;

import java.util.Random;
import java.util.Scanner;

public class RelacionArraysMatrices9 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rn = new Random();
        int n;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        int [] array = new int [n];
        System.out.println("--------------");
        for (int i = 0; i < array.length; i++) {
            array[i] = rn.nextInt(100);
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }
    
}
