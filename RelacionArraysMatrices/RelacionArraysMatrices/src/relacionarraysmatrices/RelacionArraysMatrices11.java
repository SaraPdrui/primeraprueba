package relacionarraysmatrices;

import java.util.Random;
import java.util.Scanner;

public class RelacionArraysMatrices11 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rn = new Random();
        int n, i;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        int [] array = new int [n];
        System.out.println("Primer array");
        for (i = 0; i < array.length; i++) {
            array[i] = rn.nextInt(3);
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        System.out.println("--------------");
        System.out.println("Segundo array");
        System.out.println(imprimeArray(repetidos(array)));
    }
    
    public static int [] repetidos(int [] array) {
        int iteradorRepetidos = 0, i = 0, j = 0;
        int [] rep = new int [array.length];
        for (i = 1; i < array.length; i++) {
            //Recorrer el array de repetidos, y si array[i] está dentro, no hacer el for de j
            for (j = 0; j < rep.length; j++) {
                if (array[i] == rep[j]) {
                    break;
                }
            }
            for (j = 0; j < i; j++) {
                if (array[i] == array[j]) {
                    rep[iteradorRepetidos] = array[i];
                    iteradorRepetidos++;
                    break;
                }
            }
        }
        return rep;
    }
    
    public static String imprimeArray(int [] array) {
        String devolver = "";
        for (int i = 0; i < array.length; i++) {
            devolver += array[i] + " ";
        }
        return devolver;
    }
    
}
