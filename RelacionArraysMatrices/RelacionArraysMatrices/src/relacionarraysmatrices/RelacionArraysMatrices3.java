package relacionarraysmatrices;

import java.util.Scanner;

public class RelacionArraysMatrices3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        int [] array = new int [n];
        System.out.println("--------------");
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }
    
}
