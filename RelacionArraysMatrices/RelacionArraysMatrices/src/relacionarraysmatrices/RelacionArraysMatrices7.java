package relacionarraysmatrices;

import java.util.Scanner;

public class RelacionArraysMatrices7 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Introduce un numero:");
        n = sc.nextInt();
        int [] array = new int [n];
        System.out.println("--------------");
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        System.out.println("El sumatorio de los numeros que conforman el array es: " + suma(array));
    }
    
    
    public static int suma(int [] array) {
        int sumatorio = 0;
        for (int i = 0; i < array.length; i++) {
        	sumatorio += array[i];
        }
        return sumatorio;
    }
    
}
